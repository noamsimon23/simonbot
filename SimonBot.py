import math

def do_turn(pw):
    

    max = []  #0 - index of source, 1 - index of dest, 2 - value, 3 - ships

    allValues = []
    my_planets = pw.my_planets()
    neutral_planets = pw.neutral_planets()
    enemy_planets = pw.enemy_planets()
    
    not_mine = neutral_planets + enemy_planets
    for i in range(0, len(my_planets)):
        values = []
        for j in range(0, len(not_mine)):
            values.append(not_mine[j].num_ships() + not_mine[j].growth_rate() * (1000 - pw.turn_number()) / getWayLength(my_planets[i].x(), my_planets[i].y(), not_mine[j].x(), not_mine[j].y()))
        allValues.append(values)
        
    attacked = [False] * len(not_mine)
    
    for i in range(0, len(my_planets)):
        
        max = maximumOfPlanet(pw, allValues, my_planets, not_mine, i)
        if max != [] and not attacked[max[1]]:
            attacked[max[1]] = True
            pw.issue_order(my_planets[max[0]], not_mine[max[1]], max[3])
        
def getWayLength(x1 , y1 , x2 , y2):
    return ((x1 - x2)**2+(y1 - y2)**2)**0.5    

      
def maximumOfPlanet(pw, allValues, my_planets, not_mine,i):
    max = []
    
    for j in range(0, len(allValues[i])):
        shipsRequiered = shipsToSend(pw, my_planets[i], not_mine[j])
        if (not myFleetsOnWay(pw, not_mine[j])) and (my_planets[i].num_ships() > shipsRequiered + 1):  
            
            if max == []:
                max.append(i)
                max.append(j)
                max.append(allValues[i][j])
                max.append(shipsRequiered)
            elif max[2] < allValues[i][j]:
                max.append(i)
                max.append(j)
                max.append(allValues[i][j])
                max.append(shipsRequiered)

    return max
    
            
def fleetsOnWay(pw, planet):
    for f in pw.fleets():
       if f.destination_planet() == planet:
           return f
    return 0       


def shipsToSend(pw, source, dest):
    fleets = fleetsOnWay(pw, dest)
    if dest.owner() == 0:
        if fleets != 0:
            if int(math.ceil(fleets.turn_remaining())) > int(math.ceil(pw.distance(source, dest))):
                ships = dest.num_ships() + (int(math.ceil(fleets.turn_remaining())) - int(math.ceil(pw.distance(source, dest))))*dest.growth_rate() + 1
            else:
                ships = max(dest.num_ships() + 1, dest.num_ships() + 1 + (int(math.ceil(fleets.turn_remaining())) - int(math.ceil(pw.distance(source, dest))))*dest.growth_rate())
        else:
            ships = dest.num_ships() + 1
    else:
        ships = dest.num_ships() + 1 + int(math.ceil(pw.distance(source, dest))) * dest.growth_rate()
                
    return ships                

def myFleetsOnWay(pw, planet):
    
    for f in pw.my_fleets():
        if f.destination_planet() == planet.planet_id():
           return True
    return False
    